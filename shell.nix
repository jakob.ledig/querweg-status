with import <nixpkgs> {};
mkShell {
   buildInputs = [
     nodePackages.npm
     nodejs
     elmPackages.elm
     elmPackages.elm-live
     elmPackages.elm-format
     elmPackages.elm-test
     python
   ];
}



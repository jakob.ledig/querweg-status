module PosixUtils exposing (after, compare, dateString, minus, monthNumber, paddedWithZero, posixFromInts, timeWithOffset)

import Time exposing (Month(..), Posix, posixToMillis, utc)


monthNumber : Month -> Int
monthNumber month =
    case month of
        Jan ->
            1

        Feb ->
            2

        Mar ->
            3

        Apr ->
            4

        May ->
            5

        Jun ->
            6

        Jul ->
            7

        Aug ->
            8

        Sep ->
            9

        Oct ->
            10

        Nov ->
            11

        Dec ->
            12


timeWithOffset : Posix -> Int -> Int -> Int -> Int -> Posix
timeWithOffset date days hours minutes seconds =
    let
        s =
            1000

        m =
            s * 60

        h =
            m * 60

        d =
            h * 24
    in
    Time.posixToMillis date
        |> (+) (seconds * s)
        |> (+) (minutes * m)
        |> (+) (hours * h)
        |> (+) (days * d)
        |> Time.millisToPosix


paddedWithZero : String -> String
paddedWithZero str =
    if String.length str >= 2 then
        str

    else
        "0" ++ str


dateString : Time.Posix -> String
dateString date =
    (Time.toDay utc date |> String.fromInt |> paddedWithZero)
        ++ "."
        ++ (Time.toMonth utc date |> monthNumber |> String.fromInt |> paddedWithZero)
        ++ "., "
        ++ (Time.toHour utc date |> String.fromInt |> paddedWithZero)
        ++ ":"
        ++ (Time.toMinute utc date |> String.fromInt |> paddedWithZero)


minus : Posix -> Posix -> Posix
minus a b =
    posixToMillis a - posixToMillis b |> Time.millisToPosix


compare : Posix -> Posix -> Order
compare a b =
    if posixToMillis a < posixToMillis b then
        LT

    else if posixToMillis a == posixToMillis b then
        EQ

    else
        GT


after : Posix -> Posix -> Bool
after a b =
    compare a b /= LT


{-
Mind that providing 1 for days results in January 2nd, not first
-}
posixFromInts : Int -> Int -> Int -> Int -> Posix
posixFromInts days hours minutes seconds =
    let
        s =
            1000

        m =
            s * 60

        h =
            m * 60

        d =
            h * 24
    in
    Time.millisToPosix (days * d + hours * h + minutes * m + seconds * s)

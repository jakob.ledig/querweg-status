module TidesWater exposing (Measurement, MeasurementDisplays, Model(..), Msg(..), Tendency(..), Tide(..), Water(..), availabilityThreshold, columnForWater, dateDecoder, getMeasurements, headline, highTide, init, isMeasurementAfter, isMeasurementBefore, lastAvailabiltiySwap, latestMeasurement, lowTide, main, measurementDecoder, renderMeasurements, stationId, tendency, tendencyForList, tendencyIcon, tideIcon, twelveAndAHalfHoursOfMeasurements, update, view, viewMeasurements, waterAvailabilityRow, waterByComparison, watersForMeasurements)

import Browser
import Element exposing (Element, column, fill, layout, padding, paragraph, row, spacing, table, text)
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import FeatherIcons
import Http
import Iso8601
import Json.Decode exposing (Decoder, andThen, fail, field, float, string, succeed)
import List exposing (foldl, head)
import List.Extra exposing (find)
import Maybe.FlatMap exposing (flatMap, flatMap2)
import PosixUtils exposing (after, dateString, timeWithOffset)
import String exposing (fromFloat)
import Time exposing (..)



-- MAIN


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , view = \model -> layout [] (view model)
        , update = update
        , subscriptions = subscriptions
        }



-- MODEL


type Model
    = Failure
    | Loading
    | Success (List Measurement)


init : () -> ( Model, Cmd Msg )
init _ =
    ( Loading, getMeasurements )



-- UPDATE


type Msg
    = LoadMeasurements
    | GotMeasurements (Result Http.Error (List Measurement))


update : Msg -> Model -> ( Model, Cmd Msg )
update msg _ =
    case msg of
        LoadMeasurements ->
            ( Loading, getMeasurements )

        GotMeasurements result ->
            case result of
                Ok measurements ->
                    ( Success measurements, Cmd.none )

                Err _ ->
                    ( Failure, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Time.every (60 * 1000) (\_ -> LoadMeasurements)



-- VIEW


headline : String -> Element Msg
headline string =
    Element.row
        [ Region.heading 2
        , Font.size 32
        , Font.center
        , padding 25
        ]
        [ text string ]


view : Model -> Element Msg
view model =
    column []
        [ headline "Wasser"
        , paragraph [ padding 25 ] [ text "Tidezeiten und Verfügbarkeit von Wasser aus der Beregnungsgemeinschaft im Querweg" ]
        , column [ padding 25 ] [ viewMeasurements model ]
        ]


viewMeasurements : Model -> Element Msg
viewMeasurements model =
    case model of
        Failure ->
            row []
                [ text "Keine Pegelstände gefunden"
                , Input.button [] { onPress = Just LoadMeasurements, label = text "Erneut versuchen" }
                ]

        Loading ->
            text "Lade..."

        Success measurements ->
            row [] [ twelveAndAHalfHoursOfMeasurements measurements |> renderMeasurements ]


renderMeasurements : List Measurement -> Element Msg
renderMeasurements measurements =
    watersForMeasurements measurements
        |> Maybe.map
            (\w ->
                [ columnForWater w.earlyPast
                , columnForWater w.latePast
                , columnForWater w.currentTide
                , columnForWater w.earlyFuture
                , columnForWater w.lateFuture
                , waterAvailabilityRow measurements
                ]
            )
        |> Maybe.map
            (\res ->
                column []
                    [ table
                        [ spacing 8 ]
                        { data = res
                        , columns =
                            [ { header = text "", width = fill, view = \r -> r.key }
                            , { header = text "", width = fill, view = \r -> r.value }
                            ]
                        }
                    ]
            )
        |> Maybe.withDefault (text "Probleme bei der Berechnung")


waterAvailabilityRow : List Measurement -> { key : Element msg, value : Element msg }
waterAvailabilityRow measurements =
    let
        icon =
            latestMeasurement measurements
                |> Maybe.map (\m -> m.value >= availabilityThreshold)
                |> Maybe.map
                    (\a ->
                        if a == True then
                            FeatherIcons.check

                        else
                            FeatherIcons.x
                    )
                |> Maybe.map (FeatherIcons.toHtml [])
                |> Maybe.map Element.html
    in
    { key = text "Wasser verfügbar:", value = icon |> Maybe.withDefault (text "Fehler bei der Ermittlung") }


columnForWater : Water -> { key : Element msg, value : Element msg }
columnForWater tide =
    case tide of
        PastTide m t ->
            { key = text (dateString m.time), value = row [] [ tideIcon t, text (" " ++ fromFloat m.value ++ "cm") ] }

        CurrentTide m t ->
            { key = text ("aktuell (" ++ dateString m.time ++ ")"), value = row [] [ tendencyIcon t, text (" " ++ fromFloat m.value ++ "cm") ] }

        FutureTide time t ->
            { key = text (dateString time), value = row [] [ tideIcon t ] }


watersForMeasurements : List Measurement -> Maybe MeasurementDisplays
watersForMeasurements measurements =
    let
        high =
            Maybe.map (\h -> PastTide h High) (highTide measurements)

        low =
            Maybe.map (\l -> PastTide l Low) (lowTide measurements)

        earlyPast =
            flatMap2 (waterByComparison isMeasurementBefore) high low

        latePast =
            flatMap2 (waterByComparison isMeasurementAfter) high low

        current =
            Maybe.map2 (\m t -> CurrentTide m t) (latestMeasurement measurements) (tendencyForList measurements)

        earlyFuture =
            case earlyPast of
                Just (PastTide measurement tide) ->
                    FutureTide (nextTideTime measurement.time) tide |> Just

                _ ->
                    Nothing

        lateFuture =
            case latePast of
                Just (PastTide measurement tide) ->
                    FutureTide (nextTideTime measurement.time) tide |> Just

                _ ->
                    Nothing
    in
    Maybe.map5
        (\ep lp c ef lf ->
            { earlyPast = ep, latePast = lp, currentTide = c, earlyFuture = ef, lateFuture = lf }
        )
        earlyPast
        latePast
        current
        earlyFuture
        lateFuture


type alias MeasurementDisplays =
    { earlyPast : Water
    , latePast : Water
    , currentTide : Water
    , earlyFuture : Water
    , lateFuture : Water
    }


type Water
    = PastTide Measurement Tide
    | CurrentTide Measurement Tendency
    | FutureTide Posix Tide


type Tide
    = High
    | Low


type Tendency
    = Up
    | Down


tideIcon : Tide -> Element msg
tideIcon t =
    if t == High then
        FeatherIcons.arrowUp |> FeatherIcons.toHtml [] |> Element.html

    else
        FeatherIcons.arrowDown |> FeatherIcons.toHtml [] |> Element.html


tendencyIcon : Tendency -> Element msg
tendencyIcon t =
    if t == Up then
        FeatherIcons.arrowUpRight |> FeatherIcons.toHtml [] |> Element.html

    else
        FeatherIcons.arrowDownRight |> FeatherIcons.toHtml [] |> Element.html


twelveAndAHalfHoursOfMeasurements : List Measurement -> List Measurement
twelveAndAHalfHoursOfMeasurements measurements =
    let
        twelveAndAHalfHoursOldMeasurement =
            Maybe.map (\m -> { m | time = timeWithOffset m.time 0 -12 -30 0 }) (latestMeasurement measurements)
    in
    case twelveAndAHalfHoursOldMeasurement of
        Just sixHours ->
            List.filter (\m -> isMeasurementAfter m sixHours) measurements

        Nothing ->
            []


latestMeasurement : List Measurement -> Maybe Measurement
latestMeasurement measurements =
    Maybe.map
        (\m ->
            foldl
                (\x y ->
                    if after x.time y.time then
                        x

                    else
                        y
                )
                m
                measurements
        )
        (head measurements)


lastAvailabiltiySwap : List Measurement -> Maybe Posix
lastAvailabiltiySwap measurements =
    latestMeasurement measurements
        |> Maybe.map (\m -> m.value >= availabilityThreshold)
        |> flatMap
            (\a ->
                if a == True then
                    find (\m -> m.value < availabilityThreshold) measurements

                else
                    find (\m -> m.value >= availabilityThreshold) measurements
            )
        |> Maybe.map (\m -> m.time)


waterByComparison : (Measurement -> Measurement -> Bool) -> Water -> Water -> Maybe Water
waterByComparison comparison a b =
    case ( a, b ) of
        ( PastTide ma _, PastTide mb _ ) ->
            if comparison ma mb then
                Just a

            else
                Just b

        _ ->
            Nothing


isMeasurementAfter : Measurement -> Measurement -> Bool
isMeasurementAfter a b =
    after a.time b.time


isMeasurementBefore : Measurement -> Measurement -> Bool
isMeasurementBefore a b =
    not (isMeasurementAfter a b)


highTide : List Measurement -> Maybe Measurement
highTide measurements =
    List.sortBy .value measurements |> List.reverse |> List.head


lowTide : List Measurement -> Maybe Measurement
lowTide measurements =
    List.sortBy .value measurements |> List.head


tendencyForList : List Measurement -> Maybe Tendency
tendencyForList measurements =
    Maybe.map2 (\h l -> tendency h l) (highTide measurements) (lowTide measurements)


tendency : Measurement -> Measurement -> Tendency
tendency high low =
    if after high.time low.time then
        Down

    else
        Up


availabilityThreshold : Float
availabilityThreshold =
    450


nextTideTime : Posix -> Posix
nextTideTime time =
    timeWithOffset time 0 12 30 0



-- HTTP


stationId : String
stationId =
    "6b05e8dc-3089-497a-a602-f40c4283d8bb"


getMeasurements : Cmd Msg
getMeasurements =
    Http.get
        { url = "https://www.pegelonline.wsv.de/webservices/rest-api/v2/stations/" ++ stationId ++ "/W/measurements.json?start=P1D"
        , expect = Http.expectJson GotMeasurements measurementDecoder
        }


type alias Measurement =
    { value : Float
    , time : Time.Posix
    }


measurementDecoder : Decoder (List Measurement)
measurementDecoder =
    Json.Decode.list
        (Json.Decode.map2 Measurement
            (field "value" float)
            dateDecoder
        )


dateDecoder : Decoder Time.Posix
dateDecoder =
    field "timestamp"
        (string
            |> andThen
                (\str ->
                    case Iso8601.toTime str of
                        Err _ ->
                            fail "Could not parse date"

                        Ok date ->
                            -- adding time to compensate for being 2 hours ahead of utc
                            succeed <| timeWithOffset date 0 2 0 0
                )
        )

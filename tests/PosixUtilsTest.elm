module PosixUtilsTest exposing (suite)

import Expect
import PosixUtils exposing (..)
import Test exposing (..)
import Time


suite : Test
suite =
    describe "Date related tests"
        [ describe "Posix arithmetic"
            [ test "adding two hours to Unix starting time" <|
                \_ ->
                    timeWithOffset zeroHours 0 2 0 0
                        |> dateString
                        |> Expect.equal "01.01., 02:00"
            , test "removing two hours from Unix starting time + 4" <|
                \_ ->
                    timeWithOffset fourHours 0 -2 0 0
                        |> dateString
                        |> Expect.equal "01.01., 02:00"
            , test "adding a minute to Unix starting time" <|
                \_ ->
                    timeWithOffset zeroHours 0 0 1 0
                        |> dateString
                        |> Expect.equal "01.01., 00:01"
            , test "adding seventy minutes to Unix starting time" <|
                \_ ->
                    timeWithOffset zeroHours 0 0 70 0
                        |> dateString
                        |> Expect.equal "01.01., 01:10"
            , test "adding a day to Unix starting time" <|
                \_ ->
                    timeWithOffset zeroHours 1 0 0 0
                        |> dateString
                        |> Expect.equal "02.01., 00:00"
            ]
        , test "creating a Posix entity from ints works" <|
            \_ ->
                posixFromInts 20 1 12 13
                    |> dateString
                    |> Expect.equal "21.01., 01:12"
        , test "generating a date string" <|
            \_ ->
                dateString zeroHours
                    |> Expect.equal "01.01., 00:00"
        , test "subtraction works as expected" <|
            \_ ->
                minus fourHours fourHours
                    |> Expect.equal zeroHours
        , test "`after` works as expected" <|
            \_ ->
                after fourHours zeroHours
                    |> Expect.equal True
        , test "compare works as expected" <|
            \_ ->
                PosixUtils.compare zeroHours fourHours
                    |> Expect.equal LT
        ]


zeroHours : Time.Posix
zeroHours =
    Time.millisToPosix 0


fourHours : Time.Posix
fourHours =
    Time.millisToPosix (4 * 60 * 60 * 1000)

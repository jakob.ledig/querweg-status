module TidesWaterTests exposing (suite)

import Expect
import List exposing (reverse)
import PosixUtils exposing (posixFromInts, timeWithOffset)
import Test exposing (..)
import TidesWater exposing (..)
import Time


suite : Test
suite =
    describe "Business logic"
        [ test "finding the latest measurement" <|
            \_ ->
                latestMeasurement measurementsTestData
                    |> Expect.equal
                        (Just { value = 23.0, time = Time.millisToPosix (14 * 60 * 60 * 1000) })
        , test "truncating to twelve and a half hours of measurements" <|
            \_ ->
                twelveAndAHalfHoursOfMeasurements measurementsTestData
                    |> Expect.equal
                        [ { value = 11.0, time = Time.millisToPosix (2 * 60 * 60 * 1000) }
                        , { value = 12.0, time = Time.millisToPosix (3 * 60 * 60 * 1000) }
                        , { value = 13.0, time = Time.millisToPosix (4 * 60 * 60 * 1000) }
                        , { value = 14.0, time = Time.millisToPosix (5 * 60 * 60 * 1000) }
                        , { value = 15.0, time = Time.millisToPosix (6 * 60 * 60 * 1000) }
                        , { value = 16.0, time = Time.millisToPosix (7 * 60 * 60 * 1000) }
                        , { value = 17.0, time = Time.millisToPosix (8 * 60 * 60 * 1000) }
                        , { value = 18.0, time = Time.millisToPosix (9 * 60 * 60 * 1000) }
                        , { value = 19.0, time = Time.millisToPosix (10 * 60 * 60 * 1000) }
                        , { value = 20.0, time = Time.millisToPosix (11 * 60 * 60 * 1000) }
                        , { value = 21.0, time = Time.millisToPosix (12 * 60 * 60 * 1000) }
                        , { value = 22.0, time = Time.millisToPosix (13 * 60 * 60 * 1000) }
                        , { value = 23.0, time = Time.millisToPosix (14 * 60 * 60 * 1000) }
                        ]
        , test "the right values are picked for display" <|
            \_ ->
                twelveAndAHalfHoursOfMeasurements measurementsTestData
                    |> watersForMeasurements
                    |> Expect.equal
                        (Just
                            { earlyPast = PastTide { value = 11.0, time = Time.millisToPosix (2 * 60 * 60 * 1000) } Low
                            , latePast = PastTide { value = 23.0, time = Time.millisToPosix (14 * 60 * 60 * 1000) } High
                            , currentTide = CurrentTide { value = 23.0, time = Time.millisToPosix (14 * 60 * 60 * 1000) } Down
                            , earlyFuture = FutureTide (timeWithOffset (Time.millisToPosix (2 * 60 * 60 * 1000)) 0 12 30 0) Low
                            , lateFuture = FutureTide (timeWithOffset (Time.millisToPosix (14 * 60 * 60 * 1000)) 0 12 30 0) High
                            }
                        )
        , test "finding the first tide" <|
            \_ ->
                let
                    earlier =
                        PastTide { value = 12.0, time = Time.millisToPosix (3 * 60 * 60 * 1000) } Low

                    later =
                        PastTide { value = 23.0, time = Time.millisToPosix (14 * 60 * 60 * 1000) } High
                in
                waterByComparison isMeasurementBefore earlier later
                    |> Expect.equal (Just earlier)
        , test "finding the later tide" <|
            \_ ->
                let
                    earlier =
                        PastTide { value = 12.0, time = Time.millisToPosix (3 * 60 * 60 * 1000) } Low

                    later =
                        PastTide { value = 23.0, time = Time.millisToPosix (14 * 60 * 60 * 1000) } High
                in
                waterByComparison isMeasurementAfter earlier later
                    |> Expect.equal (Just later)
        , test "finding the swapping tide if water is available" <|
            \_ ->
                let
                    ms =
                        reverse
                            [ measurement 350 0 0 0 0
                            , measurement 400 1 0 0 0
                            , measurement 410 2 0 0 0
                            , measurement 470 3 0 0 0
                            ]
                in
                lastAvailabiltiySwap ms
                    |> Expect.equal (Just (posixFromInts 2 0 0 0))
        , test "finding the swapping tide if water is not available" <|
            \_ ->
                let
                    ms =
                        reverse
                            [ measurement 470 0 0 0 0
                            , measurement 460 1 0 0 0
                            , measurement 400 2 0 0 0
                            , measurement 350 3 0 0 0
                            ]
                in
                lastAvailabiltiySwap ms
                    |> Expect.equal (Just (posixFromInts 1 0 0 0))
        ]


measurement : Float -> Int -> Int -> Int -> Int -> Measurement
measurement value days hours minutes seconds =
    { value = value, time = posixFromInts days hours minutes seconds }


measurementsTestData : List Measurement
measurementsTestData =
    [ { value = 10.0, time = Time.millisToPosix (1 * 60 * 60 * 1000) }
    , { value = 11.0, time = Time.millisToPosix (2 * 60 * 60 * 1000) }
    , { value = 12.0, time = Time.millisToPosix (3 * 60 * 60 * 1000) }
    , { value = 13.0, time = Time.millisToPosix (4 * 60 * 60 * 1000) }
    , { value = 14.0, time = Time.millisToPosix (5 * 60 * 60 * 1000) }
    , { value = 15.0, time = Time.millisToPosix (6 * 60 * 60 * 1000) }
    , { value = 16.0, time = Time.millisToPosix (7 * 60 * 60 * 1000) }
    , { value = 17.0, time = Time.millisToPosix (8 * 60 * 60 * 1000) }
    , { value = 18.0, time = Time.millisToPosix (9 * 60 * 60 * 1000) }
    , { value = 19.0, time = Time.millisToPosix (10 * 60 * 60 * 1000) }
    , { value = 20.0, time = Time.millisToPosix (11 * 60 * 60 * 1000) }
    , { value = 21.0, time = Time.millisToPosix (12 * 60 * 60 * 1000) }
    , { value = 22.0, time = Time.millisToPosix (13 * 60 * 60 * 1000) }
    , { value = 23.0, time = Time.millisToPosix (14 * 60 * 60 * 1000) }
    ]
